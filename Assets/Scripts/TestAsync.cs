using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Cysharp.Threading.Tasks;
using TMPro;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class TestAsync : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI logText;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            Debug.Log($"{0}번 키가 눌림.");
            StartAsync(0).Forget();
        } 
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            Debug.Log($"{1}번 키가 눌림.");
            StartAsync(1).Forget();
        } 
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            StartAsync(2).Forget();
        } 
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            StartAsync(3).Forget();
        } 
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            StartAsync(4).Forget();
        } 
        
    }

    private async UniTaskVoid StartAsync(int number)
    {
        switch (number)
        {
            case 1: // 비동기(동기 프로그램을 포함한)
            {
                logText.text = "";
                logText.text = logText.text + $"비동기 방식으로 식사 준비 시작\n";
                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();

                Egg egg = await (new Cooking()).MakeEggAsync();
                logText.text = logText.text + $"달걀 재료 준비 완료: {stopwatch.ElapsedMilliseconds}, {egg.GetHashCode()}\n";

                Rice rice = await (new Cooking()).MakeRiceAsync();
                logText.text = logText.text + $"밥 준비 완료: {stopwatch.ElapsedMilliseconds}, {rice.GetHashCode()}\n";

                Soup soup = await (new Cooking()).MakeSoupAsync();
                logText.text = logText.text + $"국 준비 완료: {stopwatch.ElapsedMilliseconds},  {soup.GetHashCode()}\n";
                
                stopwatch.Stop();

                logText.text = logText.text + $"시간: {stopwatch.ElapsedMilliseconds}\n";
                logText.text = logText.text + $"비동기 방식으로 식사(김밥) 준비 완료";
            }
                break;
            case 2: // 비동기(함께 실행)
            {
                logText.text = "";
                logText.text = logText.text + $"비동기(함께실행)방식으로 식사 준비 시작\n";
                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();

                // 3개의 Async 메서드가 동시 실행
                UniTask<Rice> riceTask = (new Cooking()).MakeRiceAsync();
                UniTask<Soup> soupTask = (new Cooking()).MakeSoupAsync();
                UniTask<Egg> eggTask = (new Cooking()).MakeEggAsync();

                Rice rice = await riceTask;
                logText.text = logText.text + $"식탁에 밥 준비 완료: {stopwatch.ElapsedMilliseconds}, {rice.GetHashCode()}\n";
                Soup soup = await soupTask;
                logText.text = logText.text + $"식탁에 국 준비 완료: {stopwatch.ElapsedMilliseconds}, {soup.GetHashCode()}\n";
                Egg egg = await eggTask;
                logText.text = logText.text + $"식탁에 달걀 준비 완료: {stopwatch.ElapsedMilliseconds}, {egg.GetHashCode()}\n";

                stopwatch.Stop();

                logText.text = logText.text + $"총 걸린 시간: {stopwatch.ElapsedMilliseconds}\n";
                logText.text = logText.text + $"비동기 방식으로 식사 준비 완료\n";
            }
                break;
            case 3: // 비동기(모두 완료되는 시점)
            {
                logText.text = "";
                logText.text = logText.text + $"비동기(모두 완료시점)방식으로 식사 준비 시작\n";
                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();

                // 3개의 Async 메서드가 동시 실행
                UniTask<Rice> riceTask = (new Cooking()).MakeRiceAsync();
                UniTask<Soup> soupTask = (new Cooking()).MakeSoupAsync();
                UniTask<Egg> eggTask = (new Cooking()).MakeEggAsync();

                // 모든 작업이 다 완료될 때까지 대기
                await UniTask.WhenAll(riceTask, soupTask, eggTask);

                logText.text = logText.text + $"식탁에 모든 식사 준비 완료\n";

                stopwatch.Stop();

                logText.text = logText.text + $"총 걸린 시간: {stopwatch.ElapsedMilliseconds}\n";
                logText.text = logText.text + $"비동기 방식으로 식사 준비 완료";
            }
                break;
            case 4: // 비동기(먼저 끝난 작업 확인) 
            {
                logText.text = "";
                logText.text = logText.text + $"비동기 (먼저 끝난 작업확인)방식으로 식사 준비 시작\n";
                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();

                // 3개의 Async 메서드가 동시 실행
                UniTask<Rice> rTask = (new Cooking()).MakeRiceAsync();
                UniTask<Soup> sTask = (new Cooking()).MakeSoupAsync();
                UniTask<Egg> eTask = (new Cooking()).MakeEggAsync();

                // 하나라도 작업이 끝나면 확인
                var allTasks = new List<UniTask> { rTask, sTask, eTask };
                int taskCount = allTasks.Count;

                var result = await UniTask.WhenAny(rTask, sTask, eTask);

                while (allTasks.Any()) // 작업이 하나라도 있으면 실행
                {
                    var finished = await UniTask.WhenAny(allTasks);

                    if (finished == 0)
                    {
                        //Rice rice = await rTask;
                        logText.text = logText.text + $"밥 준비 완료, {stopwatch.ElapsedMilliseconds}\n";
                    }
                    else if (finished == 1)
                    {
                        //Soup soup = await sTask;
                        logText.text = logText.text + $"국 준비 완료, {stopwatch.ElapsedMilliseconds}\n";
                    }
                    else
                    {
                        //Egg egg = await eTask;
                        logText.text = logText.text + $"달걀 준비 완료, {stopwatch.ElapsedMilliseconds}\n";
                    }
                    //allTasks.Remove(finished); // 끝난 작업은 리스트에서 제거
                }

                stopwatch.Stop();

                logText.text = logText.text + $"총 걸린 시간: {stopwatch.ElapsedMilliseconds}\n";
                logText.text = logText.text + $"비동기 방식으로 식사 준비 완료";
            }
                break;
            default: // 동기(Sync)
            {
                //스레드가 차단되어 Update의 Debug 로그도 나중에 한번에 찍힌다.
                logText.text = "";
                logText.text = logText.text + $"동기 방식으로 식사 준비 시작\n";
                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();

                //[1] 밥 만들기
                Rice rice = (new Cooking()).MakeRice(); // 스레드 차단: true
                logText.text = logText.text + $"밥 준비 완료 - {stopwatch.ElapsedMilliseconds}, {rice.GetHashCode()}\n";

                //[2] 국 만들기
                Soup soup = (new Cooking()).MakeSoup();
                logText.text = logText.text + $"국 준비 완료 - {stopwatch.ElapsedMilliseconds}, {soup.GetHashCode()}\n";
                
                //[3] 달걀 만들기
                Egg egg = (new Cooking()).MakeEgg();
                logText.text = logText.text + $"달걀 준비 완료 - {stopwatch.ElapsedMilliseconds}, {egg.GetHashCode()}\n";

                stopwatch.Stop();

                logText.text = logText.text + $"총 걸린 시간: {stopwatch.ElapsedMilliseconds}\n";
                logText.text = logText.text + $"동기 방식으로 식사 준비 완료";
            }
                break;
        }
    }
}