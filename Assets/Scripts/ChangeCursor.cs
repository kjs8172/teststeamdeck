using UnityEngine;

public class NewBehaviourScript : MonoBehaviour
{
    [SerializeField] private Texture2D cursorImg;

    private void Start()
    {
        Cursor.SetCursor(cursorImg, Vector2.zero, CursorMode.ForceSoftware);
        Cursor.lockState = CursorLockMode.Confined; // 커서의 이동반경을 제한.
        Cursor.lockState = CursorLockMode.None; 
    }
}

