using System;
using System.Collections.Generic;
using System.Threading;
using Cysharp.Threading.Tasks;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
   [SerializeField] private Button btnQuit;
   [SerializeField] private Transform background;
   [SerializeField] private Transform changDisplay;
   [SerializeField] private GameObject pfChangeDisplayBtn;
   [SerializeField] private GameObject pfBlock;
   [SerializeField] private TextMeshProUGUI timerText;
   [SerializeField] private TextMeshProUGUI resolutionText;


   [SerializeField]private Button btnResolution0;
   [SerializeField]private Button btnResolution1;
   [SerializeField]private Button btnResolution2;
   [SerializeField]private Button btnResolution3;
   [SerializeField]private Button btnResolution4;
   [SerializeField]private Button btnResolution5;
   [SerializeField]private Button btnExit;

   [SerializeField]private Button btnImageOrigin;
   [SerializeField]private Button btnImageHalf;
   [SerializeField]private Button btnImageQuarter;

   [SerializeField] private TMP_Dropdown resolutionDropdown;
   [SerializeField] private Toggle fullscreenToggle;
   [SerializeField] private Toggle vSyncToggle;
   
   [SerializeField] private Button[] changeImageBtns;
   [SerializeField] private GameObject[] characterImages;
   [SerializeField] private List<GameObject> changeDisplayBtns;

   [SerializeField] private TextMeshProUGUI logText;
   [SerializeField] private TextMeshProUGUI fpsText;

   private readonly CancellationTokenSource source = new();
   private readonly CancellationTokenSource timeoutToken = new();

   private int time;
   private List<string> options = new List<string>();
   
   private int currentMonitorIndex;

   private List<Resolution> resolutions;
   
   private float deltaTime = 0.0f;
   private float msec;
   private float fps;
   
   private int ResolutionIndex 
   {
      get => PlayerPrefs.GetInt("ResolutionIndex", 0);
      set => PlayerPrefs.SetInt("ResolutionIndex", value);
   }

   private bool IsFullscreen 
   {
      get => PlayerPrefs.GetInt("IsFullscreen", 1) == 1;
      set => PlayerPrefs.SetInt("IsFullscreen", value ? 1 : 0);
   }
   
   private bool IsVSyncActivate 
   {
      get => PlayerPrefs.GetInt("IsVSyncActivate", 1) == 1;
      set => PlayerPrefs.SetInt("IsVSyncActivate", value ? 1 : 0);
   }

   
   private void Awake()
   {
      btnQuit.onClick.AddListener(OnQuit);
      Camera mainCamera = Camera.main;

      resolutionDropdown.onValueChanged.AddListener(DropdownOptionChanged);
      fullscreenToggle.onValueChanged.AddListener(FullscreenToggleChanged);
      vSyncToggle.onValueChanged.AddListener(VSyncToggleChanged);
      

      SetResolutionButtons();
      SetChangeImageButtons();
      
      btnExit.onClick.AddListener(() =>
      {
         Debug.Log("프로그램 종료~~~~~");
         OnQuit();
      });
   }

   private void Start()
   {
      InitResolution();
      SetResolution();
      
      //SetMultiDisplayInfo(); //듀얼모니터에서 두번째 모니터에 표시하려고.

      // TimerAsync().Forget();
      // TimeoutExampleAsync().Forget();
      ChangeCharacterImage(0);
      //MakeBackgroundImageForTest();
   }

   private void InitResolution()
   {
      resolutions = new List<Resolution>(Screen.resolutions);
      resolutions.Reverse();
      
      // 16:9 경우만 가져온다.
      // if (is16v9) 
      // {
      //    resolutions = resolutions.FindAll(x => (float)x.width / x.height == 16f / 9);
      // }
      
      foreach (var resolution in resolutions)
      {
         string option = $"{resolution.width} x {resolution.height} {resolution.refreshRate}Hz";
         options.Add(option);
      }
      
      resolutionDropdown.ClearOptions();
      resolutionDropdown.AddOptions(options);
   }

   private void SetResolution()
   {
      resolutionDropdown.value = ResolutionIndex;
      fullscreenToggle.isOn = IsFullscreen;
      vSyncToggle.isOn = IsVSyncActivate;

      resolutionDropdown.RefreshShownValue();

      DropdownOptionChanged(ResolutionIndex);
   }
   
   private void DropdownOptionChanged(int _resolutionIndex) 
   {
      ResolutionIndex = _resolutionIndex;
      Resolution resolution = resolutions[_resolutionIndex];
      logText.text += $"\n{resolution.width}x{resolution.height}";
      Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
      Application.targetFrameRate = Int32.Parse(resolution.refreshRate.ToString());
   }
   
   private void FullscreenToggleChanged(bool _isFull)
   {
      IsFullscreen = _isFull;
      Screen.fullScreen = _isFull;
   }
   
   private void VSyncToggleChanged(bool _isActive)
   {
      IsVSyncActivate = _isActive;
      QualitySettings.vSyncCount = _isActive? 1 : 0;
   }


   private readonly List<DisplayInfo> displayInfo = new List<DisplayInfo>();

   private void SetMultiDisplayInfo()
   {
      logText.text = $"디스플레이 갯수 ==== {Display.displays.Length}개\n";

      // if (Display.displays.Length > 1)
      // {
      //    for (int i = 1; i < Display.displays.Length; i++)
      //    {
      //       Display.displays[i].Activate();  // 이걸하면 윈도우모드에서 창의 크기를 줄일수 없음.
      //    }
      // }

      Screen.GetDisplayLayout(displayInfo);
      if (displayInfo.Count <= 1)
      {
         SetChangeDisplay(0);
         return;
      }
      
      for (int i = 0; i < displayInfo.Count; i++)
      {
         GameObject button = Instantiate(pfChangeDisplayBtn, changDisplay, true);
         button.name = $"Display{i}";
         button.GetComponent<ChangeDisplay>().SetDisplayButton(i, SetChangeDisplay);
         button.GetComponent<ChangeDisplay>().SetDisplayButtonActivate(false);
         changeDisplayBtns.Add(button);
         Debug.Log($"{i}: {displayInfo[i].name} - {displayInfo[i].width}x{displayInfo[i].height} ");
      }
      SetChangeDisplay(0);
   }
   
   private void SetChangeDisplay(int _index)
   {
      SetChangeDisplayButtonActivate(_index);
      int selectedWidth = 3840;
      int selectedHeight = 2160;
      Vector2Int selectedPosition = GetWindowPosition(_index, selectedWidth, selectedHeight);
      Screen.MoveMainWindowTo(displayInfo[_index], selectedPosition);
      // Screen.SetResolution(selectedWidth, selectedHeight, false);
      Screen.SetResolution(selectedWidth, selectedHeight, FullScreenMode.Windowed);
   }

   private void SetChangeDisplayButtonActivate(int _index)
   {
      foreach (var button in changeDisplayBtns)
      {
         button.GetComponent<ChangeDisplay>().SetDisplayButtonActivate(false);
      }

      if (displayInfo.Count > 1)
      {
         changeDisplayBtns[_index].GetComponent<ChangeDisplay>().SetDisplayButtonActivate(true);
      }

      currentMonitorIndex = _index;
      GetCurrentDisplay();
   }

   private void GetCurrentDisplay()
   {
      Resolution resolution = Screen.resolutions[Screen.resolutions.Length - 1];
      Debug.Log($"{resolution.width} x {resolution.height} {resolution.refreshRateRatio}");
      DisplayInfo currentDisplayInfo = displayInfo[currentMonitorIndex];
      logText.text += $"{currentMonitorIndex}: " + $"{currentDisplayInfo.name}" +
                      $"- {currentDisplayInfo.width}x{currentDisplayInfo.height}\n";
      Debug.Log($"{currentMonitorIndex}: " + $"{currentDisplayInfo.name} " +
                $"- {currentDisplayInfo.width}x{currentDisplayInfo.height} ");
   }

   private Vector2Int GetWindowPosition(int _index, int _width, int _height)
   {
      int monitorWidth = displayInfo[_index].width;
      int monitorHeight = displayInfo[_index].height;
      
      int x = monitorWidth / 2 - _width / 2;
      int y = monitorHeight / 2 - _height / 2;

      return new Vector2Int(x, y);
   }

   private void SetResolutionButtons()
   {
      btnResolution0.onClick.AddListener(() =>
      {
         Debug.Log("풀스크린으로 변경~~~~~");
         Screen.SetResolution(3840, 2160, FullScreenMode.FullScreenWindow);
      });
      
      btnResolution1.onClick.AddListener(() =>
      {
         Debug.Log("Exclusive 풀스크린으로 변경~~~~~");
         Screen.SetResolution(3840, 2160, FullScreenMode.ExclusiveFullScreen);
      });
      btnResolution2.onClick.AddListener(() =>
      {
         Debug.Log("MaximizedWindow 모드(3840x2160) 변경~~~~~");
         Screen.SetResolution(3840, 2160, FullScreenMode.MaximizedWindow);
      });
      btnResolution3.onClick.AddListener(() =>
      {
         Debug.Log("Windowed 모드(3840x2160) 변경~~~~~");
         Screen.SetResolution(3840, 2160, FullScreenMode.Windowed);
      });
      btnResolution4.onClick.AddListener(() =>
      {
         Debug.Log("윈도우 모드(1920x1080) 변경~~~~~");
         Screen.SetResolution(1920, 1080, FullScreenMode.MaximizedWindow);
      });
      btnResolution5.onClick.AddListener(() =>
      {
         Debug.Log("윈도우 모드(1920x1080) 변경~~~~~");
         Screen.SetResolution(1920, 1080, FullScreenMode.Windowed);
      });
   }

   private void SetChangeImageButtons()
   {
      btnImageOrigin.onClick.AddListener(() =>
      {
         ChangeCharacterImage(0);
      });

      btnImageHalf.onClick.AddListener(() =>
      {
         ChangeCharacterImage(1);
      });
      btnImageQuarter.onClick.AddListener(() =>
      {
         ChangeCharacterImage(2);
      });
   }

   private void ChangeCharacterImage(int _index)
   {
      foreach (var t in characterImages)
      {
         t.gameObject.SetActive(false);
      }

      characterImages[_index].gameObject.SetActive(true);
      ChangeButtonActivate(_index);
   }
   
   private void ChangeButtonActivate(int _index)
   {
      foreach (var t in changeImageBtns)
      {
         t.GetComponent<Image>().color = Color.gray;
      }

      changeImageBtns[_index].GetComponent<Image>().color = Color.blue;
   }

   private async UniTaskVoid TimerAsync()
   {
      time = 0;
      timerText.text = time.ToString();
      while (time < 100)
      {
         await UniTask.Delay(TimeSpan.FromSeconds(1), cancellationToken: source.Token);
         time++;
         timerText.text = time.ToString();
      }
   }

   private async UniTaskVoid Wait10SecondsAsync()
   {
      await UniTask.WaitUntil(() => time == 10, cancellationToken: timeoutToken.Token);
      Debug.Log("10초가 지났다.");
   }

   private readonly CancellationTokenSource cancelToken = new();
   
    private async UniTaskVoid TimeoutExampleAsync()
    {
       timeoutToken.CancelAfterSlim(TimeSpan.FromSeconds(10));  //3초가 지나면 타임아웃처리
       try
       {
          CancellationTokenSource linkedTokenSource = CancellationTokenSource.CreateLinkedTokenSource(cancelToken.Token, timeoutToken.Token);
          await UniTask.WaitUntil(() => time >= 4, cancellationToken: linkedTokenSource.Token);
          Debug.Log("카운트가 4 이상이 되었다.");
   
       }
       catch (OperationCanceledException e)
       {
          if (timeoutToken.IsCancellationRequested)
          {
             Debug.Log("Timeout!!! 10초가 지났다.");
          }
          else if (cancelToken.IsCancellationRequested)
          {
             Debug.Log("취소를 눌렀다.");
          }
       }
    }

   

   private void Update()
   {
      resolutionText.text = $"{Screen.fullScreenMode}\n{Screen.width}x{Screen.height}";

      if (Input.GetKey(KeyCode.S))
      {
         Debug.Log("캡쳐시작~~~~~");
         ScreenCapture.CaptureScreenshot("BackGround.png");
      }
      
      if (Input.GetKey(KeyCode.Escape))
      {         
         Debug.Log("프로그램 종료~~~~~");
         OnQuit();
      }

      if (Input.GetKey(KeyCode.Alpha0))
      {         
         Debug.Log("타이머 스톱~~~~~~~~~~~~~");
         source.Cancel();
      }

      ShowFps();
   }

   private void ShowFps()
   {
      deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
      msec = deltaTime * 1000f;
      fps = 1.0f / deltaTime;

      fpsText.text = $"{msec:F1}ms({fps:F1} FPS)";
   }
   

   private void OnQuit()
   {
      Application.Quit();
   }

   private void MakeBackgroundImageForTest()
   {
      var index = 0;
      for (var i = 0; i < 9; i++)
      {
         var color1 = (i % 2 == 0) ? Color.white : Color.black;
         var color2 = (i % 2 == 0) ? Color.black : Color.white;
         
         for (int j = 0; j < 16; j++)
         {
            index++;
            GameObject tempBlock = Instantiate(pfBlock, background, true);
            tempBlock.name = index.ToString();
            tempBlock.GetComponent<Image>().color = (j % 2 == 0)? color1 : color2;
            tempBlock.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = index.ToString();
         }
      }
   }
}
