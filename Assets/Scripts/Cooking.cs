using System;
using System.Threading;
using Cysharp.Threading.Tasks;

public class Cooking
{
    private CancellationTokenSource sourceRice = new();
    private CancellationTokenSource sourceSoup = new();
    private CancellationTokenSource sourceEgg = new();

    /// <summary>
    /// 동기 방식의 밥 만들기 메서드 
    /// </summary>
    /// <returns>밥</returns>
    public Rice MakeRice()
    {
        Console.WriteLine("밥 생성중...");
        Thread.Sleep(3001);
        return new Rice();
    }

    /// <summary>
    /// 비동기 방식의 밥 만들기 메서드 
    /// </summary>
    /// <returns>밥</returns>
    public async  UniTask<Rice> MakeRiceAsync()
    {
        Console.WriteLine("밥 생성중...");
        await UniTask.Delay(millisecondsDelay:3001, cancellationToken: sourceRice.Token); //[A]
        return new Rice();
    }

 
    /// <summary>
    /// 동기 방식의 국 만들기 메서드
    /// </summary>
    /// <returns>국</returns>
    public Soup MakeSoup()
    {
        Console.WriteLine("국 생성중...");
        Thread.Sleep(2001);
        return new Soup();
    }

    /// <summary>
    /// 비동기 방식의 국 만들기 메서드
    /// </summary>
    /// <returns>국</returns>
    public async  UniTask<Soup> MakeSoupAsync()
    {
        Console.WriteLine("국 생성중...");
        await UniTask.Delay(2001, cancellationToken: sourceSoup.Token); //[B]
        return new Soup();
    }

    /// <summary>
    /// 동기 방식의 달걀 만들기 메서드
    /// </summary>
    /// <returns>달걀</returns>
    public Egg MakeEgg()
    {
        Console.WriteLine("달걀 생성중...");
        Thread.Sleep(1001);
        return new Egg();
    }

    /// <summary>
    /// 비동기 방식의 달걀 만들기 메서드
    /// </summary>
    /// <returns>달걀</returns>
    public async UniTask<Egg> MakeEggAsync()
    {
        Console.WriteLine("달걀 생성중...");
        await UniTask.Delay(TimeSpan.FromMilliseconds(2001), cancellationToken: sourceEgg.Token);
        return new Egg();
    }
}

public class Food
{
}

public class Rice : Food
{ 
    // Pass
}

public class Soup : Food
{ 
    // Pass
}

public class Egg : Food
{ 
    // Pass
}