using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ChangeDisplay : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI displayName;
    private int displayIndex;
    private Action<int> resultCallback;
    
    private void Awake()
    {
        GetComponent<Button>().onClick.AddListener(() =>
        {
            resultCallback(displayIndex);
        });
    }

    public void SetDisplayButton(int _index, Action<int> _onComplete)
    {
        resultCallback = _onComplete;
        displayIndex = _index;
        displayName.text = (displayIndex + 1).ToString();
    }

    public void SetDisplayButtonActivate(bool isActive)
    {
        GetComponent<Image>().color = isActive ? Color.magenta : Color.gray;
    }
}
