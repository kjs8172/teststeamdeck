using System;
using System.Threading;
using Cysharp.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class GetImageAsync : MonoBehaviour
{
    [SerializeField] private Button btn;
    [SerializeField] private RawImage profileImage;
    [SerializeField] private TMP_InputField inputField;

    private const string ImagePath = "https://t1.daumcdn.net/cfile/tistory/9933564D5FE4A71418";

    
    private void Start()
    {
       CancellationToken token = this.GetCancellationTokenOnDestroy();
       
        // btn.onClick.AddListener(()=>
        //     {
        //        GetImageTask().Forget();//Web이미지 불러오기;
        //     });
        
        btn.onClick.AddListener(UniTask.UnityAction(async () =>
        {
           var result = await GetWebTextureAsync();
           GetImageTask(result);
        }));

        CheckChangeInputField(token).Forget();

        Debug.Log($"오늘은 {DateTime.Now:yyyy년 MM월 dd일} {GetDay()}요일 입니다.");
    }

    private void  GetImageTask(Texture2D _texture)
    {
       if (_texture == null)
       {
          profileImage.gameObject.SetActive(false);
       }
       else
       {
          profileImage.gameObject.SetActive(true);
          profileImage.texture = _texture;
       }

       CancellationToken token = this.GetCancellationTokenOnDestroy();
       DelayAndHideImage(2000, token).Forget();
    }
    
    private async UniTask<Texture2D> GetWebTextureAsync()
    {
       UnityWebRequest request = UnityWebRequestTexture.GetTexture(ImagePath);
       await request.SendWebRequest();
    
       if (request.result is UnityWebRequest.Result.ConnectionError or UnityWebRequest.Result.ProtocolError)
       {
          Debug.Log(request.error);
       }
       else
       {
          Texture2D texture = ((DownloadHandlerTexture) request.downloadHandler).texture;
          return texture;
       }
       return null;
    }

    private async UniTaskVoid CheckChangeInputField(CancellationToken _token)
    {
       while (true)
       {
          await UniTask.WaitUntilValueChanged(
                    target: inputField,
                    monitorFunction: x => x.text,
                    cancellationToken: _token
                    );
                 
          Debug.Log($"내용이 바뀜~~~~~~~ {inputField.text}");
          await UniTask.Yield(PlayerLoopTiming.FixedUpdate, cancellationToken: _token);
       }
    }

    private async UniTaskVoid DelayAndHideImage(int _time, CancellationToken _token)
    {
       await UniTask.Delay(TimeSpan.FromMilliseconds(_time), cancellationToken: _token);
       profileImage.gameObject.SetActive(false);
    }

    private string GetDay()
    {
       return DateTime.Now.DayOfWeek
          switch
          {
             DayOfWeek.Monday => "월",
             DayOfWeek.Tuesday => "화",
             DayOfWeek.Wednesday => "수",
             DayOfWeek.Thursday => "목",
             DayOfWeek.Friday => "금",
             DayOfWeek.Saturday => "토",
             DayOfWeek.Sunday => "일",
             _ => ""  //예외일경우.
          };
    }
}
